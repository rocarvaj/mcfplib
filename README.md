# Some multicommodity flow problem instances

This is a repository of instances multicommodity flow problems.

## Instances
### C instances ##

These are instances from [here](http://www.di.unipi.it/optimize/Data/MMCF.html#NetDesMMCF). For a recent
summary on the state of these problems, visit N. Katamaya's [website](http://www2.rku.ac.jp/katayama/CND_UB_C.html).

### GT instances (F_T folder)##
These are instances used [here](http://joc.journal.informs.org/content/22/2/314.short).

Right now there are only tightly capacitated instances.

For more information about the format, see `GT-README.txt`.

## To Do
* Include R instances.
* Include F_L instances.
* Add script/library to manipulate instances.

[Rodolfo Carvajal](http://www2.isye.gatech.edu/~rcarvajal3), 2013
