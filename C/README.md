# C instances
These instances come from <http://www.di.unipi.it/optimize/Data/MMCF.html>.

## Structure of files ##
* number_of_nodes number_of_arcs number_of_commodities

* for each arc:
    from_node to_node variable_cost capacity fixed_cost any_number any_number

* for each commodity:
    from_node to_node demand (>0)

## Current results ##
N. Katamaya has a nice summary of results in his [website](http://www2.rku.ac.jp/katayama/CND_UB_C.html).

[Rodolfo Carvajal](http://www2.isye.gatech.edu/~rcarvajal3/), 2013
