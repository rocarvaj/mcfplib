F_L directory contains instances with high fixed costs that are loosely capacitated

F_T directory contains instances with high fixed costs that are tightly capacitated

FILE FORMAT:

If the first line of the file says SINGLE_PATH, then the instance is one where
commodity demand must flow on a single path.

The line NODES,X indicates that the network contains X nodes.
The next X lines then list those nodes in the format
Index,Name (although the name is always the index of those X nodes)

The line ARCS,X indicates that the network contains X arcs.
The next X arcs then list those arcs in the format
Origin,Destination,Variable Cost,Fixed Cost,Capacity

The line COMMODITIES,X indicates that the network contains X commodities.
The next X lines then list those commodities in the format
Origin,Destination,Size